# README #

COE 332 Homework 7
Bradley Smith

Charges will be given to the client for these reasons:
Cloud resources: This will allow for the server to be online for access whenever needed, but this will cost money and/or resources for me (this includes storing their data)
API creation: For the time and effort it took to make the API and any future upkeep/improvements to the API
API documentation: Documentation is necessary for the client to understand how the program works and takes time to create as well; this can be merged with the previous point