To launch the project, enter (into the terminal):
'python3 hw7.py'
To exit the project, press ctrl+c

All API functions can be accessed by the command:
'curl '/austin_pop_history[/additional_url]'
where the part in brackets represents

GET /austin_pop_history
get all Austin population history
Example:
GET /austin_pop_history
(curl /austin_pop_history)
[
  {
    "population": 553, 
    "year": 1840
  }, 
  {
    "population": 629, 
    "year": 1850
  }, 
  {
    "population": 3494, 
    "year": 1860
  }, 
  {
    "population": 4428, 
    "year": 1870
  }, 
  {
    "population": 11013, 
    "year": 1880
  }, 
...
]


GET /austin_pop_history?start=<start_year>&end=<end_year>
get austin population history data from a timeframe between two years (either is optional)
Example:
GET /austin_pop_history?start=1900&end=1910
(curl '/austin_pop_history?start=1900&end=1910')
[
  {
    "population": 22258, 
    "year": 1900
  }, 
  {
    "population": 25299, 
    "year": 1905
  }, 
  {
    "population": 29860, 
    "year": 1910
  }, 
  {
    "population": 32368, 
    "year": 1915
  },
  ...
    {
    "population": 526128, 
    "year": 1995
  }, 
  {
    "population": 548043, 
    "year": 1996
  }, 
  {
    "population": 567566, 
    "year": 1997
  }, 
  {
    "population": 613458, 
    "year": 1998
  }, 
  {
    "population": 629769, 
    "year": 1999
  }, 
  {
    "population": 656562, 
    "year": 2000
  }
]


GET /austin_pop_history/<int:id>
get the population data point corresponding to the id
Example:
GET /austin_pop_history/15
(curl /austin_pop_history/15)
{
  "population": 101289, 
  "year": 1945
}

GET /austin_pop_history/<time>/<int:value>
get austin population history data point from a specific time
Example:
GET /austin_pop_history/year/1945
(curl /austin_pop_history/year/1945)
{
  "population": 101289, 
  "year": 1945
}

GET /austin_pop_history/mean?start=<start_year>&end=<end_year>
get the average population of austin (start and end are optional; providing either will set a bound for the data to be displayed)
Example:
GET /austin_pop_history/mean?start=1900&end=2000
(curl '/austin_pop_history/mean?start=1900&end=2000')
300813.4528301887


POST /austin_pop_history/new_data/<int:year>&<int:population>
add another data point to the data set
Example:
GET /austin_pop_history/new_data/2017&950715
(curl /austin_pop_history/new_data/2017&950715)
Success

